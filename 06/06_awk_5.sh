#!/bin/bash

awk '
  $2 <= numbervalue { print $1, "--"; }
  $2 > numbervalue { print $1, "++" ; }
' numbervalue="$1" input.txt
