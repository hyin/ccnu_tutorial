#!/bin/bash

awk 'BEGIN {print "==>"} 
     /DST/ {x+=1;} 
     END {print x; print NR; 
          print FILENAME}' input.txt
