void exponential_random(){
  gROOT->ProcessLine(".x ../13/lhcbStyle.C");
  TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
  TH1D* hist_exp = new TH1D("hist_exp", "", 100, 0, 20);

  for(int i = 0; i < 1000000; i++){
     hist_exp->Fill(gRandom->Exp(3.0));
  }
  hist_exp->Draw();
}
