# Instructions

The codes in this package are used to follow the data analysis tutuorials,
which are prepared for the graduate course of CCNU.

Each sub-directory is corresponding to the .pdf file in twiki. Links for these docments can be found in the twiki, as:

- Shell: [link](https://twiki.cern.ch/twiki/bin/view/Sandbox/CcnuLhcbShell) (03 -- 07)
- C++: [link](https://twiki.cern.ch/twiki/bin/view/Sandbox/CcnuLhcbCplusplus) (08 -- 12)
- ROOT: [link](https://twiki.cern.ch/twiki/bin/view/Sandbox/CcnuLhcbRoot) (13 -- 16)
- Advanced ROOT: [link](https://twiki.cern.ch/twiki/bin/view/Sandbox/CcnuLhcbAdvanceRoot) (17 -- 21)

If you have any suggestions regarding to any of docments, please contact me, via [hyin AT cern.ch].

The first version was upolad to this package at Jan. 15th, 2023.