#ifndef __READ_FILE_HH__
#define __READ_FILE_HH__

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
using namespace std;

// class name
class Read_file{
public:
  // class constructor
  Read_file();
  // class destructor
  ~Read_file();

  // other functions
  void ReadFileToVector(ifstream &input, vector<double> &px, 
                        vector<double> &py, vector<double> &pz, 
                        vector<double> &pe);

private:
 
};
#endif
