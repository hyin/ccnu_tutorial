#include "Read_file.hpp"

Read_file::Read_file(){}
Read_file::~Read_file(){}

void Read_file::ReadFileToVector(ifstream &input, 
                                 vector<double> &px, 
                                 vector<double> &py, 
                                 vector<double> &pz, 
                                 vector<double> &pe){
  while (!input.eof()){
    string name_tmp;
    double px_tmp, py_tmp, pz_tmp, pe_tmp;

    input >> name_tmp >> px_tmp >> py_tmp >> pz_tmp >> pe_tmp;
    px.push_back(px_tmp);    py.push_back(py_tmp);
    pz.push_back(pz_tmp);    pe.push_back(pe_tmp);
  }
}
