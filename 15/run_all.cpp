#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <unistd.h>  //getopt
#include <cstdlib>   //exit
#include "Ana_mass.hpp"
#include "Read_file.hpp"
#include "TFile.h"
#include "TTree.h"
using namespace std;

int main(int argc, char** argv){
  if(!(argc == 3 || argc == 2)){
    cout<<"=========================================================="<<endl;
    cout<<"  Wrong input parameters !!!"<<endl
	<<"  Please check help information:"<<endl
	<<"    ./a.out -h"<<endl;
    cout<<"=========================================================="<<endl;
    exit(0);
  } 
  //  Read in parameters                                             //
  string input_text_name;
  int c;
  while( (c=getopt(argc, argv, "hi:")) != -1 ){
    switch(c){
    case 'h':
      cout<<"=========================================================="<<endl;
      cout<<"  Usage:"<<endl
	  <<"     ./a.out -i input-text-name "<<endl<<endl;
      cout<<"  Example: "<<endl
	  <<"     ./a.out -i data_input.txt"<<endl<<endl;
      cout<<"=========================================================="<<endl;
      exit(0);
      break;
    case 'i':
      input_text_name  = optarg;
      break;
    }
  }
  //  print out input parameters                                     //
  cout<<"  --> The input text file is  : "<<input_text_name<<endl;

  // input from a file
  ifstream input(input_text_name.c_str());

  // vector init
  vector<double> px_vec;  vector<double> py_vec;
  vector<double> pz_vec;  vector<double> pe_vec;
  px_vec.clear(); py_vec.clear();
  pz_vec.clear(); pe_vec.clear();

  // class init
  Ana_mass* mass_algo = new Ana_mass;
  Read_file* read_file = new Read_file;

  read_file->ReadFileToVector(input, px_vec, py_vec, pz_vec, pe_vec); 
 
  // mass vector 
  vector<double> mass_vec;
  mass_vec.clear();

  for(int i = 0; i < px_vec.size(); i++){
    double mass = mass_algo->calMass(px_vec[i], py_vec[i], pz_vec[i], pe_vec[i]);
    mass_vec.push_back(mass);
  }

  //TFile
  TFile* file_group = new TFile("Tuple_bmass_forTest.root", "recreate");
  // tree name is "Tuple"
  TTree* group_tree = new TTree("Tuple", "Tuple");
  double bmass_tmp;
  double bpx_tmp;
  double bpy_tmp;
  double bpz_tmp;
  double bpe_tmp;

  //branch setting
  group_tree->Branch("Mass", &bmass_tmp, "Mass/D");
  group_tree->Branch("Px", &bpx_tmp, "Px/D");
  group_tree->Branch("Py", &bpy_tmp, "Py/D");
  group_tree->Branch("Pz", &bpz_tmp, "Pz/D");
  group_tree->Branch("Pe", &bpe_tmp, "Pe/D");

  //fill mass information into branch
  for(int i=0; i<mass_vec.size(); i++){
    bmass_tmp = mass_vec[i];
    bpx_tmp = px_vec[i];
    bpy_tmp = py_vec[i];
    bpz_tmp = pz_vec[i];
    bpe_tmp = pe_vec[i];
    group_tree->Fill();
  }

  //write information to root file
  file_group->Write();
  //close root file
  file_group->Close();

  cout<<"End"<<endl;
}
