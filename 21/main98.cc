// main02.cc is a part of the PYTHIA event generator.
// Copyright (C) 2020 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; z production; tevatron;

// This is a simple test program. It fits on one slide in a talk.
// It studies the pT_Z spectrum at the Tevatron.

#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main() {
  // Generator. Process selection. Tevatron initialization. Histogram.
  Pythia pythia;
  pythia.readString("Beams:idB = -2212");
  pythia.readString("Beams:eCM = 1960.");
  pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  pythia.readString("PhaseSpace:mHatMin = 80.");
  pythia.readString("PhaseSpace:mHatMax = 120.");
  pythia.readString("23:onMode = off");
  pythia.readString("23:onIfAny = 13 -13");
  pythia.readString("PartonLevel:MPI = off");
  pythia.init();
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < 10; ++iEvent) {
    if (!pythia.next()) continue;

    // find mother particle: Z0
    int idxZ = -1;
    for (int i = pythia.event.size() - 1; i > 0; i--) {
      if (pythia.event[i].idAbs() == 23) {
        idxZ = i;
        break;
      }
    }
    if (idxZ == -1) {
      cout << "Error: Could not find Z" << endl;
      continue;
    }

    // get the first two daughters
    bool first_generation = true;
    int idxmu1 = idxZ;
    int idxmu2 = idxZ;
    while(true) {
      if(first_generation){
        int daughter1 = pythia.event[idxmu1].daughter1();
        int daughter2 = pythia.event[idxmu1].daughter2();
        if(daughter1 == 0) break;
        else{
          idxmu1  = daughter1;
          idxmu2  = daughter2;
        }
        first_generation = false;
        break;
      }
    }

    // get the final state muons
    while(true) {
      if(pythia.event[idxmu1].idAbs() == 13 &&
         !pythia.event[idxmu1].isFinal() &&
         pythia.event[idxmu1].isCharged()){
        int d1 = pythia.event[idxmu1].daughter1();
        int d2 = pythia.event[idxmu1].daughter2();
        if(pythia.event[d1].idAbs() == 13) idxmu1 = d1;
        else idxmu1 = d2;
      }else break;
    }
    while(true) {
      if(pythia.event[idxmu2].idAbs() == 13 &&
         !pythia.event[idxmu2].isFinal() &&
         pythia.event[idxmu2].isCharged()){
        int d1 = pythia.event[idxmu2].daughter1();
        int d2 = pythia.event[idxmu2].daughter2();
        if(pythia.event[d1].idAbs() == 13) idxmu2 = d1;
        else idxmu2 = d2;
      }else break;
    }

    // print out information
    std::cout<<pythia.event[idxZ].id()<<"  "<<pythia.event[idxZ].px()<<"  "<<pythia.event[idxZ].py()<<"  "<<pythia.event[idxZ].pz()<<"  "<<pythia.event[idxZ].e()<<"  "<<pythia.event[idxZ].isFinal()<<std::endl;
    std::cout<<pythia.event[idxmu1].id()<<"  "<<pythia.event[idxmu1].px()<<"  "<<pythia.event[idxmu1].py()<<"  "<<pythia.event[idxmu1].pz()<<"  "<<pythia.event[idxmu1].e()<<"  "<<pythia.event[idxmu1].isFinal()<<std::endl;
    std::cout<<pythia.event[idxmu2].id()<<"  "<<pythia.event[idxmu2].px()<<"  "<<pythia.event[idxmu2].py()<<"  "<<pythia.event[idxmu2].pz()<<"  "<<pythia.event[idxmu2].e()<<"  "<<pythia.event[idxmu2].isFinal()<<std::endl;
   std::cout<<" 000 "<<std::endl;
  }
  pythia.stat();
  return 0;
}
