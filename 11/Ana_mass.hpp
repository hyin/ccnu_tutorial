#ifndef __ANA_MASS_HH__
#define __ANA_MASS_HH__

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
using namespace std;

// class name
class Ana_mass{
public:
  // class constructor
  Ana_mass();
  // class destructor
  ~Ana_mass();

  // other functions
  double calMass(double px, double py, double pz, double pe);

private:
 
};
#endif
