#include "Ana_mass.hpp"

Ana_mass::Ana_mass(){}
Ana_mass::~Ana_mass(){}

double Ana_mass::calMass(double px, double py, double pz, double pe){

  double mass = sqrt(pow(pe, 2) - pow(px, 2) -
	   	     pow(py, 2) - pow(pz, 2));
  return mass;
}
