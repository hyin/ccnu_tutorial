#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <unistd.h>  //getopt
#include <cstdlib>   //exit
#include "Ana_mass.hpp"

using namespace std;

int main(int argc, char** argv){
  if(!(argc == 3 || argc == 2)){
    cout<<"=========================================================="<<endl;
    cout<<"  Wrong input parameters !!!"<<endl
	<<"  Please check help information:"<<endl
	<<"    ./a.out -h"<<endl;
    cout<<"=========================================================="<<endl;
    exit(0);
  } 
  //  Read in parameters                                             //
  string input_text_name;
  int c;
  while( (c=getopt(argc, argv, "hi:")) != -1 ){
    switch(c){
    case 'h':
      cout<<"=========================================================="<<endl;
      cout<<"  Usage:"<<endl
	  <<"     ./a.out -i input-text-name "<<endl<<endl;
      cout<<"  Example: "<<endl
	  <<"     ./a.out -i data_input.txt"<<endl<<endl;
      cout<<"=========================================================="<<endl;
      exit(0);
      break;
    case 'i':
      input_text_name  = optarg;
      break;
    }
  }
  //  print out input parameters                                     //
  cout<<"  --> The input text file is  : "<<input_text_name<<endl;

  // input from a file
  ifstream input(input_text_name.c_str());

  // vector init
  vector<double> px_vec;  vector<double> py_vec;
  vector<double> pz_vec;  vector<double> pe_vec;
  px_vec.clear(); py_vec.clear();
  pz_vec.clear(); pe_vec.clear();

  // c++ default print out
  while (!input.eof()){
    string name_tmp;
    double px_tmp, py_tmp, pz_tmp, pe_tmp;

    input >> name_tmp >> px_tmp >> py_tmp >> pz_tmp >> pe_tmp;
    px_vec.push_back(px_tmp);    py_vec.push_back(py_tmp);
    pz_vec.push_back(pz_tmp);    pe_vec.push_back(pe_tmp);
  }
 
  // mass vector 
  vector<double> mass_vec;
  mass_vec.clear();

  // class init
  Ana_mass* mass_algo = new Ana_mass;

  for(int i = 0; i < px_vec.size(); i++){
     double mass = mass_algo->calMass(px_vec[i], py_vec[i], pz_vec[i], pe_vec[i]);
     mass_vec.push_back(mass);
  }

  // if selection
  for(int i = 0; i < mass_vec.size(); i++){
    if(mass_vec[i] < 5230){
      cout<<mass_vec[i]<<" ### < 5300"<<endl;
    }else if(mass_vec[i] < 5250){
      cout<<mass_vec[i]<<" $$$ < 5250"<<endl;
    }else{
      cout<<mass_vec[i]<<" --- > 5250 "<<endl;
    }
  }
}
