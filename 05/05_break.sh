#!/bin/bash

x=0
while [ $x -lt 10 ] ; do
  echo $x
  if [ $x -gt 5 ] ; then
    break
  fi
  x=$(( $x + 1 ))
done
