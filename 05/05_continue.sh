#!/bin/bash

for FILE in ABC ; do
  if [ ! -f $FILE ] ; then
    echo "ERROR: $FILE is not a file."
    continue
  fi
done
