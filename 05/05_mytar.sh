#!/bin/bash

USAGE="Usage: $0 [-c|-t] [file|directory]"
case $1 in 
  -t) TARGS="-tvf $2" ;;
  -c) TARGS="-cvf $2.tar $2" ;;
   *) echo "$USAGE"
      exit 0;;
esac
echo "tar "$TARGS
