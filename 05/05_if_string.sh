#!/bin/bash

FRUIT_BASKET=""
if [ -z "${FRUIT_BASKET}" ]; then
  echo "Fruit basket is empty"
else 
  echo "Fruit basket is NOT empty"
fi

FRUIT_BASKET="apple"
if [ ${FRUIT_BASKET} = "apple" ]; then
  echo "Fruit basket has ${FRUIT_BASKET}"
else
  echo "Fruit basket does not have apple"
fi
