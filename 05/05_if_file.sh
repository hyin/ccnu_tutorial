#!/bin/bash

if [ -d /etc/ ] ; then
  echo "/etc/ is a directory!!"
fi

if [ -f /etc/ssh/ssh_config ] ; then
  echo "/etc/ssh/ssh_config is a normal file!!"
fi

if [ -s /etc/ssh/ssh_config ] ; then
  echo "/etc/ssh/ssh_config has content!!"
else 
  echo "/etc/ssh/ssh_config has NO content!!"
fi

