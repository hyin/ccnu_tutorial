#!/bin/bash

USAGE="Usage: $0 [-i] [filename] [-o] [filename]"
while getopts "i:o:" OPTION ; do
  case $OPTION in
    i) INFILE="${OPTARG}" ;;
    o) OUTFILE="${OPTARG}" ;;
   \?) echo "USAGE" 
       exit 1 ;;
  esac
done
echo "Input file is $INFILE, output file is $OUTFILE"
