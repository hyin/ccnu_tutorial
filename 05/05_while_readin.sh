#!/bin/bash

KEYBROAD_INPUT=""
while [ -z ${KEYBROAD_INPUT} ] ; do
  echo "Enter the input value for the parameters x: "
  read KEYBROAD_INPUT
  if [ -n ${KEYBROAD_INPUT} ] ; then
    echo "The input value for x is ${KEYBROAD_INPUT}"
  fi
done
