#!/bin/bash

if [ $? -eq 0 ]; then
  echo "Command was successful."
else
  echo "An error was encountered."
  exit
fi
