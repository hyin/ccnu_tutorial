#include <iostream>
using namespace std;
  
enum BREED { YORKIE, CAIRN, DANDIE, SHETLAND, DOBERMAN, LAB };
  
class Mammal{
public:
  // constructors
  Mammal() { cout << "Mammal constructor ..." << endl; }
  ~Mammal() { cout << "Mammal destructor ..." << endl; }

  // other methods
  void speak() const { cout << "Mammal sound!" << endl; }
  void sleep() const { cout << "Shhh. I'm sleeping." << endl; }

protected:
  int age;
  int weight;
};
  
class Dog : public Mammal{
public:
  // constructors
  Dog() { cout << "Dog constructor ..." << endl; }
  ~Dog() { cout << "Dog destructor ..." << endl; }

  // other methods
  void wagTail() { cout << "Tail wagging ..." << endl; }
  void begForFood() { cout << "Begging for food ..." << endl; }
  void speak() const { cout << "Woof!" << endl; }

private:
  BREED breed;
};
  
int main(){
  Mammal bigAnimal;
  Dog fido;
  bigAnimal.speak();
  fido.speak();
  return 0;
}
