#include <iostream>
using namespace std;

class Dog{
public:
  void speak()const { cout << "Woof!" << endl; }
  void move() const { cout << "Walking to heel ..." << endl; }
  void eat() const { cout << "Gobbling food ..." << endl; }
  void growl() const { cout << "Grrrrr" << endl; }
  void whimper() const { cout << "Whining noises ..." << endl; }
  void rollOver() const { cout << "Rolling over ..." << endl; }
  void playDead() const
    { cout << "Is this the end of Little Caesar?" << endl; }
};

typedef void (Dog::*PDF)()const;

int main(){
  const int MaxFuncs = 7;
  PDF dogFunctions[MaxFuncs] =
    {   &Dog::speak,
        &Dog::move,
        &Dog::eat,
        &Dog::growl,
        &Dog::whimper,
        &Dog::rollOver,
        &Dog::playDead
    };

  Dog* pDog =0;
  int method;
  bool fQuit = false;

  while (!fQuit){
    cout << "(0) Quit (1) Speak (2) Move (3) Eat (4) Growl";
    cout << " (5) Whimper (6) Roll Over (7) Play Dead: ";
    cin >> method;
    if (method == 0){
      fQuit = true;
      break;
    }
    else{
      pDog = new Dog;
      (pDog->*dogFunctions[method - 1])();
      delete pDog;
    }
  }
  return 0;
}
