#include <iostream>
using namespace std;
  
enum BREED { YORKIE, CAIRN, DANDIE, SHETLAND, DOBERMAN, LAB };
  
class Mammal{
public:
  // constructors
  Mammal();
  Mammal(int age);
  ~Mammal();

  // accessors
  int getAge() const { return age; }
  void setAge(int newAge) { age = newAge; }
  int getWeight() const { return weight; }
  void setWeight(int newWeight) { weight = newWeight; }

  // other methods
  void speak() const { cout << "Mammal sound!" << endl; }
  void sleep() const { cout << "Shhh. I'm sleeping." << endl; }

protected:
  int age;
  int weight;
};
  
class Dog : public Mammal{
public:
  // constructors
  Dog();
  Dog(int age);
  Dog(int age, int weight);
  Dog(int age, BREED breed);
  Dog(int age, int weight, BREED breed);
  ~Dog();

  // accessors
  BREED getBreed() const { return breed; }
  void setBreed(BREED newBreed) { breed = newBreed; }

  // other methods
  void wagTail() { cout << "Tail wagging ..." << endl; }
  void begForFood() { cout << "Begging for food ..." << endl; }

private:
  BREED breed;
};
  
Mammal::Mammal():
age(1),
weight(5){
  cout << "Mammal constructor ..." << endl;
}
  
Mammal::Mammal(int age):
age(age),
weight(5){
  cout << "Mammal(int) constructor ..." << endl;
}
  
Mammal::~Mammal(){
  cout << "Mammal destructor ..." << endl;
}
  
Dog::Dog():
Mammal(),
breed(YORKIE){
  cout << "Dog constructor ..." << endl;
}
  
Dog::Dog(int age):
Mammal(age),
breed(YORKIE){
  cout << "Dog(int) constructor ..." << endl;
}
  
Dog::Dog(int age, int newWeight):
Mammal(age),
breed(YORKIE){
  weight = newWeight;
  cout << "Dog(int, int) constructor ..." << endl;
}
  
Dog::Dog(int age, int newWeight, BREED breed):
Mammal(age),
breed(breed){
  weight = newWeight;
  cout << "Dog(int, int, BREED) constructor ..." << endl;
}
  
Dog::Dog(int age, BREED newBreed):
Mammal(age),
breed(newBreed){
  cout << "Dog(int, BREED) constructor ..." << endl;
}
  
Dog::~Dog(){
  cout << "Dog destructor ..." << endl;
}

int main(){
  Dog fido;
  Dog rover(5);
  Dog buster(6, 8);
  Dog yorkie (3, YORKIE);
  Dog dobbie (4, 20, DOBERMAN);
  fido.speak();
  rover.wagTail();
  cout << "Yorkie is " 
      << yorkie.getAge() << " years old" << endl;
  cout << "Dobbie weighs " 
      << dobbie.getWeight() << " pounds" << endl;
  return 0;
}
