#include <iostream>
using namespace std;

class Mammal{
public:
  Mammal():age(1) { cout << "Mammal constructor ..." << endl; }
  virtual ~Mammal() { cout << "Mammal destructor ..." << endl; }
  virtual void speak() const { cout << "Mammal speak!" << endl; }
protected:
  int age;
};

class Cat: public Mammal{
public:
  Cat() { cout << "Cat constructor ..." << endl; }
  ~Cat() { cout << "Cat destructor ..." << endl; }
  void speak() const { cout << "Meow!" << endl; }
  void purr() const { cout << "Rrrrrrrrrrr!" << endl; }
};

class Dog: public Mammal{
public:
  Dog() { cout << "Dog constructor ..." << endl; }
  ~Dog() { cout << "Dog destructor ..." << endl; }
  void speak() const { cout << "Woof!" << endl; }
};

int main(){
  const int numberMammals = 3;
  Mammal* zoo[numberMammals];
  Mammal* pMammal;
  int choice, i;
  for (i = 0; i < numberMammals; i++){
    cout << "(1)Dog (2)Cat: ";
    cin >> choice;
    if (choice == 1)
      pMammal = new Dog;
    else
      pMammal = new Cat;

    zoo[i] = pMammal;
  }

  cout << endl;

  for (i = 0; i < numberMammals; i++){
    zoo[i]->speak();

    Cat *pRealCat =  dynamic_cast<Cat *> (zoo[i]);

    if (pRealCat)
      pRealCat->purr();
    else
      cout << "Uh oh, not a cat!" << endl;

    delete zoo[i];
    cout << endl;
  }

  return 0;
}
