#include <iostream>
using namespace std;
  
class Mammal{
public:
  void move() const { cout << "Mammal moves one step" << endl; }
  void move(int distance) const 
      { cout << "Mammal moves " << distance <<" steps" << endl; }
protected:
  int age;
  int weight;
};
  
class Dog : public Mammal{
public:
  void move() const { cout << "Dog moves 5 steps" << endl; }
}; // you may receive a warning that you are hiding a function!
  
int main(){
  Mammal bigAnimal;
  Dog fido;
  bigAnimal.move();
  bigAnimal.move(2);
  fido.move();
  fido.Mammal::move(10);
  return 0;
}
