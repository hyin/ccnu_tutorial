#include <iostream>
using namespace std;

enum COLOR { Red, Green, Blue, Yellow, White, Black, Brown } ;

class Animal{ // common base to both horse and bird

public:
  Animal(int);
  virtual ~Animal() { cout << "Animal destructor ..." << endl; }
  virtual int getAge() const { return age; }
  virtual void setAge(int newAge) { age = newAge; }
  virtual void sleep() const = 0;
  virtual void eat() const = 0;
  virtual void reproduce() const = 0;
  virtual void move() const = 0;
  virtual void speak() const = 0;
private:
  int age;
};

Animal::Animal(int newAge):
age(newAge){
  cout << "Animal constructor ..." << endl;
}

class Mammal : public Animal{
public:
  Mammal(int newAge):Animal(newAge)
    { cout << "Mammal constructor ..." << endl;}
  virtual ~Mammal() { cout << "Mammal destructor ..." << endl;}
  virtual void reproduce() const
    { cout << "Mammal reproduction depicted ..." << endl; }
};

class Fish : public Animal{
public:
  Fish(int newAge):Animal(newAge)
    { cout << "Fish constructor ..." << endl;}
  virtual ~Fish()
    { cout << "Fish destructor ..." << endl;  }
  virtual void sleep() const
    { cout << "Fish snoring ..." << endl; }
  virtual void eat() const
    { cout << "Fish feeding ..." << endl; }
  virtual void reproduce() const
    { cout << "Fish laying eggs ..." << endl; }
  virtual void move() const
    { cout << "Fish swimming ..." << endl;   }
  virtual void speak() const { }
};

class Horse : public Mammal{
public:
   Horse(int newAge, COLOR newColor):
     Mammal(newAge), color(newColor)
     { cout << "Horse constructor ..." << endl; }
   virtual ~Horse()
     { cout << "Horse destructor ..." << endl; }
   virtual void speak() const
     { cout << "Whinny!" << endl; }
   virtual COLOR getcolor() const
     { return color; }
   virtual void sleep() const
     { cout << "Horse snoring ..." << endl; }
   virtual void eat() const
     { cout << "Horse feeding ..." << endl; }
   virtual void move() const
     { cout << "Horse running ..." << endl;}

protected:
  COLOR color;
};

class Dog : public Mammal{
public:
   Dog(int newAge, COLOR newColor ):
     Mammal(newAge), color(newColor)
     { cout << "Dog constructor ..." << endl; }
   virtual ~Dog()
     { cout << "Dog destructor ..." << endl; }
   virtual void speak() const
     { cout << "Whoof!" << endl; }
   virtual void sleep() const
     { cout << "Dog snoring ..." << endl; }
   virtual void eat() const
     { cout << "Dog eating ..." << endl; }
   virtual void move() const
     { cout << "Dog running..." << endl; }
   virtual void reproduce() const
     { cout << "Dogs reproducing ..." << endl; }

protected:
  COLOR color;
};

int main(){
  Animal *pAnimal = 0;
  int choice;
  bool fQuit = false;

  while (1){
    cout << "(1) Dog (2) Horse (3) Fish (0) Quit: ";
    cin >> choice;

    switch (choice){
    case 1:
      pAnimal = new Dog(5, Brown);
      break;
    case 2:
      pAnimal = new Horse(4, Black);
      break;
    case 3:
      pAnimal = new Fish(5);
      break;
    default:
      fQuit = true;
      break;
    }
    if (fQuit)
      break;

    pAnimal->speak();
    pAnimal->eat();
    pAnimal->reproduce();
    pAnimal->move();
    pAnimal->sleep();
    delete pAnimal;
    cout << endl;
  }
  return 0;
}
