#include <iostream>
using namespace std;
  
class Mammal{
public:
  Mammal():age(1) {  }
  ~Mammal() { }
  virtual void speak() const { cout << "Mammal speak!" << endl; }
protected:
  int age;
};
  
class Dog : public Mammal{
public:
  void speak() const { cout << "Woof!" << endl; }
};
  
class Cat : public Mammal{
public:
  void speak()const { cout << "Meow!" << endl; }
};
  
void valueFunction(Mammal);
void ptrFunction(Mammal*);
void refFunction(Mammal&);
  
int main(){
  Mammal* ptr=0;
  int choice;
  while (1){
    bool fQuit = false;
    cout << "(1) dog (2) cat (0) quit: ";
    cin >> choice;
    switch (choice){
    case 0: 
      fQuit = true;
      break;
    case 1: 
      ptr = new Dog;
      break;
    case 2: 
      ptr = new Cat;
      break;
    default: 
      ptr = new Mammal;
      break;
    }
    if (fQuit){
      break;
    }
    ptrFunction(ptr);
    refFunction(*ptr);
    valueFunction(*ptr); 
  }
  return 0;
}
  
void ptrFunction (Mammal *pMammal){
    pMammal->speak();
}
  
void refFunction (Mammal &rMammal){
    rMammal.speak();
}

void valueFunction(Mammal mammalValue){ 
  mammalValue.speak();
}
  
