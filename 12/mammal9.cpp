#include <iostream>
using namespace std;
  
class Mammal{
public:
  Mammal():age(1) { cout << "Mammal constructor ..." << endl; }
  virtual ~Mammal() { cout << "Mammal destructor ..." << endl; }
  Mammal (const Mammal &rhs);
  virtual void speak() const { cout << "Mammal speak!" << endl; }
  virtual Mammal* clone() { return new Mammal(*this); } 
  int getAge() const { return age; }

protected:
  int age;
};
  
Mammal::Mammal (const Mammal &rhs):age(rhs.getAge()){
  cout << "Mammal copy constructor ..." << endl;
}
  
class Dog : public Mammal{
public:
  Dog() { cout << "Dog constructor ..." << endl; }
  virtual ~Dog() { cout << "Dog destructor ..." << endl; }
  Dog (const Dog &rhs);
  void speak() const { cout << "Woof!" << endl; }
  virtual Mammal* clone() { return new Dog(*this); }
};
  
Dog::Dog(const Dog &rhs):
Mammal(rhs){
  cout << "Dog copy constructor ..." << endl;
}
  
class Cat : public Mammal{
public:
  Cat() { cout << "Cat constructor ..." << endl; }
  virtual ~Cat() { cout << "Cat destructor ..." << endl; }
  Cat (const Cat&);
  void speak() const { cout << "Meow!" << endl; }
  virtual Mammal* Clone() { return new Cat(*this); }
};
  
Cat::Cat(const Cat &rhs):
Mammal(rhs){
  cout << "Cat copy constructor ..." << endl;
}
  
enum ANIMALS { MAMMAL, DOG, CAT};
const int numAnimalTypes = 3;
int main(){
  Mammal *array[numAnimalTypes];
  Mammal *ptr;
  int choice, i;
  for (i = 0; i < numAnimalTypes; i++){
    cout << "(1) dog (2) cat (3) mammal: ";
    cin >> choice;
    switch (choice){
    case DOG: 
      ptr = new Dog;
      break;
    case CAT: 
      ptr = new Cat;
      break;
    default: 
      ptr = new Mammal;
      break;
    }
    array[i] = ptr;
  }
  Mammal *otherArray[numAnimalTypes];
  for (i=0; i < numAnimalTypes; i++){
    array[i]->speak();
    otherArray[i] = array[i]->clone();
  }
  for (i=0; i < numAnimalTypes; i++){
    otherArray[i]->speak();
  }
  return 0;
}
