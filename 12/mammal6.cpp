#include <iostream>
using namespace std;
  
class Mammal{
public:
  Mammal():age(1) { cout << "Mammal constructor ..." << endl; }
  ~Mammal() { cout << "Mammal destructor ..." << endl; }
  void move() const { cout << "Mammal, move one step" << endl; }
  virtual void speak() const { cout << "Mammal speak!" << endl; }

protected:
  int age;
};
  
class Dog : public Mammal{
public:
  Dog() { cout << "Dog constructor ..." << endl; }
  ~Dog() { cout << "Dog destructor .." << endl; }
  void wagTail() { cout << "Wagging tail ..." << endl; }
  void speak() const { cout << "Woof!" << endl; }
  void move() const { cout << "Dog moves 5 steps ..." << endl; }
};
  
int main(){
  Mammal *pDog = new Dog;
  pDog->move();
  pDog->speak();
  return 0;
}
