#include <iostream>
using namespace std;
  
enum BREED { YORKIE, CAIRN, DANDIE, SHETLAND, DOBERMAN, LAB };
  
class Mammal{
public:
  // constructors
  Mammal();
  ~Mammal();

  // accessors
  int getAge() const { return age; }
  void setAge(int newAge) { age = newAge; }
  int getWeight() const { return weight; }
  void setWeight(int newWeight) { weight = newWeight; }

  // other methods
  void speak() const { cout << "Mammal sound!" << endl; }
  void sleep() const { cout << "shhh. I'm sleeping." << endl; }

protected:
  int age;
  int weight;
};
  
class Dog : public Mammal{
public:
  // constructors
  Dog();
  ~Dog();

  // accessors
  BREED getBreed() const { return breed; }
  void setBreed(BREED newBreed) { breed = newBreed; }

  // other methods
  void wagTail() { cout << "Tail wagging ..." << endl; }
  void begForFood() { cout << "Begging for food ..." << endl; }

private:
  BREED breed; 
};
  
Mammal::Mammal():
age(1),
weight(5){
  cout << "Mammal constructor ..." << endl;
}
  
Mammal::~Mammal(){
  cout << "Mammal destructor ..." << endl;
}
  
Dog::Dog():
breed(YORKIE){
  cout << "Dog constructor ..." << endl;
}
  
Dog::~Dog(){
  cout << "Dog destructor ..." << endl;
}
  
int main(){
  Dog fido; // create a dog
  fido.speak();
  fido.wagTail();
  cout << "Fido is " << fido.getAge() << " years old" << endl;
  return 0;
}
