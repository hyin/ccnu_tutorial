#include <iostream>
using namespace std;

enum BREED { YORKIE, CAIRN, DANDIE, SHETLAND, DOBERMAN, LAB };
  
class Mammal{
public:
  // constructors
  Mammal(): age(2), weight(5) {}
  ~Mammal(){}

  // accessors
  int getAge() const { return age; }
  void setAge(int newAge) { age = newAge; }
  int getWeight() const { return weight; }
  void setWeight(int newWeight) { weight = newWeight; }

  // other methods
  void speak() const { cout << "Mammal sound!" << endl; }
  void sleep() const { cout << "Shhh. I'm sleeping." << endl; }

protected:
  int age;
  int weight;
};
  
class Dog : public Mammal{
public:
  // constructors
  Dog(): breed(YORKIE) {}
  ~Dog() {}

  // accessors
  BREED getBreed() const { return breed; }
  void setBreed(BREED newBreed) { breed = newBreed; }

  // other methods
  void wagTail() { cout << "Tail wagging ..." << endl; }
  void begForFood() { cout << "Begging for food ..." << endl; }

private:
  BREED breed;
};
  
int main(){
  Dog fido;
  fido.speak();
  fido.wagTail();
  cout << "Fido is " << fido.getAge() << " years old" << endl;
  return 0;
}
