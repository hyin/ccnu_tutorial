void makeTuple(string input_text_name) {
  cout << "  --> The input text file is  : " << input_text_name <<".txt"<< endl;

  // input from a file
  ifstream input((input_text_name + ".txt").c_str());

  // TFile
  TFile* file_group = new TFile((input_text_name + ".root").c_str(), "recreate");

  // tree name is "Tuple"
  TTree* group_tree = new TTree("Tuple", "Tuple");
  double ZBoson_PX, ZBoson_PY, ZBoson_PZ, ZBoson_PE;
  double mup_PX, mup_PY, mup_PZ, mup_PE;
  double mum_PX, mum_PY, mum_PZ, mum_PE;

  // branch setting
  group_tree->Branch("ZBoson_PX", &ZBoson_PX, "ZBoson_PX/D");
  group_tree->Branch("ZBoson_PY", &ZBoson_PY, "ZBoson_PY/D");
  group_tree->Branch("ZBoson_PZ", &ZBoson_PZ, "ZBoson_PZ/D");
  group_tree->Branch("ZBoson_PE", &ZBoson_PE, "ZBoson_PE/D");
  group_tree->Branch("mup_PX", &mup_PX, "mup_PX/D");
  group_tree->Branch("mup_PY", &mup_PY, "mup_PY/D");
  group_tree->Branch("mup_PZ", &mup_PZ, "mup_PZ/D");
  group_tree->Branch("mup_PE", &mup_PE, "mup_PE/D");
  group_tree->Branch("mum_PX", &mum_PX, "mum_PX/D");
  group_tree->Branch("mum_PY", &mum_PY, "mum_PY/D");
  group_tree->Branch("mum_PZ", &mum_PZ, "mum_PZ/D");
  group_tree->Branch("mum_PE", &mum_PE, "mum_PE/D");
  
  bool finished_file = false;  
  //int index = 0;
  while( ! finished_file ) {
    int evn;
    double evt_wt;
    double Q2,that,uhat,x1,x2,flav1,flav2 ;
    input >>  evn >> evt_wt >> Q2 >> that >> uhat >> x1 >> x2 >> flav1 >> flav2;

    if( evn == 0 ) {
      finished_file = true;
      continue;
    }

    // continue text reading
    double vx,vy,vz;
    input >> vx >> vy >>vz;

    bool finished_particles = false;
    bool first_mum = false;
    bool first_mup = false;

    while( ! finished_particles && !input.eof()) {
      int id;
      input >> id;
      if( id == 0 ){
        finished_particles = true;
        group_tree->Fill();
      }
      else {
        double px,py,pz,E = 0.;
        input >> px >> py >> pz >> E;
        int origin, udk;
        input >> origin >> udk;

        if(abs(id) == 23){
          ZBoson_PX = px; ZBoson_PY = py;
          ZBoson_PZ = pz; ZBoson_PE = E;          
        }
        else if(id == 13 && !first_mum){
          mum_PX = px; mum_PY = py;
          mum_PZ = pz; mum_PE = E; 
          first_mum = true;      
        }
        else if(id == -13 && !first_mup){
          mup_PX = px; mup_PY = py;
          mup_PZ = pz; mup_PE = E;
          first_mup = true;
          //if(index < 100) cout<< px <<"  "<<py<<"  "<<pz<<"  "<<E<<endl;  
          //index++;
        }
      }
    }
    if( input.eof()) finished_file = true;
  }
  input.close();
  // write information to root file
  file_group->Write();
  // close root file
  file_group->Close();

  cout << "The output root file is "<< (input_text_name + ".root") <<" ..." << endl;
}
