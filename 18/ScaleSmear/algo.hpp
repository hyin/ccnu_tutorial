double CalChi2_1D(TH1D* h1, TH1D* h2, bool printout = false){
  // make sure they have the same binnings
  Int_t nbins = h1->GetNbinsX();
  if(h2->GetNbinsX()!=nbins){
    cout<<" *** Chi^2 calculation: different number of bins for h1 and h2 templates"<<endl;
    cout<<"     The number of bins in h1 is "<<nbins<<";"<<endl;
    cout<<"     The number of bins in h2 is "<<h2->GetNbinsX()<<" !!"<<endl;
  }
  
  double chi2 = 0.;
  for(int ibin = 1; ibin < nbins+1; ibin++) {
    double error_data = h1->GetBinError(ibin)*h1->GetBinError(ibin) + h2->GetBinError(ibin)*h2->GetBinError(ibin);
    if(error_data > 0){
      double chi2_tmp = pow(h1->GetBinContent(ibin) - h2->GetBinContent(ibin), 2)/(error_data);
      if(printout) cout<<"   ibin "<<ibin<<" , Chi2 = "<<chi2_tmp<<endl;
      chi2 += chi2_tmp;
    }
  }
  return chi2;
}

