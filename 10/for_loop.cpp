#include <iostream>
using namespace std;

int main(){
  int number;
  cout << "Enter a number: ";
  cin >> number;

  cout << "First 10 Multiples of " << number << endl;

  for (int counter = 1; counter < 11; counter++){
    cout << number * counter << "  ";
  }
  cout << endl;

  return 0;
}
