#include <iostream>
using namespace std;
float convert(float);

int main(){
  float fahrenheit;
  float celsius;

  cout << "Please enter the temperature in Fahrenheit: "; 
  cin >> fahrenheit;
  celsius = convert(fahrenheit);
  cout << "Here's the temperature in Celsius: ";
  cout << celsius << endl;

  return 0;
} 

// function to convert Fahrenheit to Celsius
float convert(float fahrenheit){
  float celsius;
  celsius = ((fahrenheit - 32) * 5) / 9;
  return celsius;
}
