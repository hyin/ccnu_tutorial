#include <iostream>
using namespace std;
  
int main(){
  char grade;
  cout << "Enter your letter grade (ABCDF): ";
  cin >> grade;
  switch (grade){
    case 'A':
      cout << "Finally!" << endl;
      break;
    case 'B':  
      cout << "You can do better!" << endl;
      break;
    case 'C':  
      cout << "I'm disappointed in you!" << endl;
      break;
    case 'D':  
      cout << "You're not smart!" << endl;
      break;
    case 'F':  
      cout << "Get out of my sight!" << endl;
      break;
    default: 
      cout << "That's not even a grade!" << endl;
      break;
  }

  return 0;
}
