#include <iostream>
using namespace std;

int findArea(int length, int width); // function prototype

int main(){
  int length;
  int width;
  int area;

  cout << "How wide is your yard?" << endl;
  cin >> width;
  cout << "How long is your yard?" << endl;
  cin >> length;

  area = findArea(length, width);

  cout << "Your yard is ";
  cout << area;
  cout << " square meter" << endl << endl;

  return 0;
}

// function definition
int findArea(int l, int w){
  return l * w;
}
