#include <iostream>
using namespace std;

int main(){

  for (int x = 0, y = 0; x < 10; x++, y++){
    cout << x * y << "  ";
  }
  cout << endl;

  int x = 0;
  int y = 0;
  for (; x < 10; x++, y+=2){
    cout << x * y << "  ";
  }
  cout << endl;

  return 0;
}
