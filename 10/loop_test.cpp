#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
using namespace std;
int main(){
  // input from a file
  ifstream input("./data_input.txt");

  // vector init
  vector<double> px_vec;  vector<double> py_vec;
  vector<double> pz_vec;  vector<double> pe_vec;
  px_vec.clear(); py_vec.clear();
  pz_vec.clear(); pe_vec.clear();

  // c++ default print out
  while (!input.eof()){
    string name_tmp;
    double px_tmp, py_tmp, pz_tmp, pe_tmp;

    input >> name_tmp >> px_tmp >> py_tmp >> pz_tmp >> pe_tmp;
    px_vec.push_back(px_tmp);    py_vec.push_back(py_tmp);
    pz_vec.push_back(pz_tmp);    pe_vec.push_back(pe_tmp);
  }
 
  // mass vector 
  vector<double> mass_vec;
  mass_vec.clear();

  for(int i = 0; i < px_vec.size(); i++){
     double mass = sqrt(pow(pe_vec[i], 2) - pow(px_vec[i], 2) -
			pow(py_vec[i], 2) - pow(pz_vec[i], 2));
     mass_vec.push_back(mass);
  }

  // if selection
  for(int i = 0; i < mass_vec.size(); i++){
    if(mass_vec[i] < 5230){
      cout<<mass_vec[i]<<" ### < 5300"<<endl;
    }else if(mass_vec[i] < 5250){
      cout<<mass_vec[i]<<" $$$ < 5250"<<endl;
    }else{
      cout<<mass_vec[i]<<" --- > 5250 "<<endl;
    }
  }
}
