#include <iostream>

using namespace std;
int main(){
  // create a type defintion
  typedef unsigned short USHORT;

  // set up width and length
  USHORT width = 5;
  USHORT length = 10;

  // create an unsigned short initialized with the
  // result of multiplying width by length
  USHORT area = width * length;

  cout << "Width: " << width << endl;
  cout << "Length: " << length << endl;
  cout << "Area: " << area << endl;

  return 0;
}
