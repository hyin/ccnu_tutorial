#include <iostream>
#include <fstream>
using namespace std;

void printbinary(const unsigned int val){  
  for(int i = 16; i >= 0; i--){  
    if(val & (1 << i))
      cout << "1";  
    else  
      cout << "0";  
  }  
  cout<<endl;
}  

int main(){  
  int num_a = 10;
  int num_b = 29;

  printf("%08x \n", num_a); // hexadecimal  
  printf("%08o \n", num_b); // octal 
  printbinary(num_b);   // binary

  return 0;
}

