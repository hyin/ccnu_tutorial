#include <iostream>

using namespace std;
int add(double x, double y){
  // add the numbers x and y together and return the sum
  return (x+y);
}

int main(){
  /* this program calls an add() function to add two different 
     sets of numbers together and display the results. The 
     add() function doesn't do anything unless it is called by
     a line in the main() function. */
  
  cout << "What is 867 + 5309?" << endl;
  cout << "The sum is " << add(867, 5309) << endl << endl;
  cout << "What is 777 + 9311?" <<endl;
  cout << "The sum is " << add(777, 9311) << endl;

  return 0;
}

