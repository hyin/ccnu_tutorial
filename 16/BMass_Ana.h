//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Jan  7 21:30:19 2023 by ROOT version 6.26/10
// from TTree Tuple/Tuple
// found on file: ../15/Tuple_bmass_forTest.root
//////////////////////////////////////////////////////////

#ifndef BMass_Ana_h
#define BMass_Ana_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <iostream>
using namespace std;

// Header file for the classes stored in the TTree if any.

class BMass_Ana : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        Mass;
   Double_t        Px;
   Double_t        Py;
   Double_t        Pz;
   Double_t        Pe;

   // List of branches
   TBranch        *b_Mass;   //!
   TBranch        *b_Px;   //!
   TBranch        *b_Py;   //!
   TBranch        *b_Pz;   //!
   TBranch        *b_Pe;   //!

   BMass_Ana(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~BMass_Ana() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

private:
   int n_events;
   int n_cuts;

   TFile* myRootFile;
   TH1D* histMass;
   TH1D* histPt;
   TH1D* histRapidity;

   ClassDef(BMass_Ana,0);
};

#endif

#ifdef BMass_Ana_cxx
void BMass_Ana::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Mass", &Mass, &b_Mass);
   fChain->SetBranchAddress("Px", &Px, &b_Px);
   fChain->SetBranchAddress("Py", &Py, &b_Py);
   fChain->SetBranchAddress("Pz", &Pz, &b_Pz);
   fChain->SetBranchAddress("Pe", &Pe, &b_Pe);
}

Bool_t BMass_Ana::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef BMass_Ana_cxx
