#define BMass_Ana_cxx
// The class definition in BMass_Ana.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("BMass_Ana.C")
// root> T->Process("BMass_Ana.C","some options")
// root> T->Process("BMass_Ana.C+")
//

#include "BMass_Ana.h"
#include <TH2.h>
#include <TStyle.h>


void BMass_Ana::Begin(TTree * /*tree*/)
{
   // The Begin() function is called at the start of the query.
   // When running with PROOF Begin() is only called on the client.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

   n_events = 0;
   n_cuts = 0;

   myRootFile = new TFile("BEvent_Information_Skimmed.root", "recreate");

   histMass = new TH1D("histMass", "", 100, 5100, 5450);
   histPt = new TH1D("histPt", "", 100, 0, 20000);
   histRapidity = new TH1D("histRapidity", "", 30, 2.0, 5.0);
   histMass->Sumw2();
   histPt->Sumw2();
   histRapidity->Sumw2();
}

void BMass_Ana::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

}

Bool_t BMass_Ana::Process(Long64_t entry)
{
   // The Process() function is called for each entry in the tree (or possibly
   // keyed object in the case of PROOF) to be processed. The entry argument
   // specifies which entry in the currently loaded tree is to be processed.
   // It can be passed to either BMass_Ana::GetEntry() or TBranch::GetEntry()
   // to read either all or the required parts of the data. When processing
   // keyed objects with PROOF, the object is already loaded and is available
   // via the fObject pointer.
   //
   // This function should contain the "body" of the analysis. It can contain
   // simple or elaborate selection criteria, run algorithms on the data
   // of the event and typically fill histograms.
   //
   // The processing can be stopped by calling Abort().
   //
   // Use fStatus to set the return value of TTree::Process().
   //
   // The return value is currently not used.
   fChain->GetTree()->GetEntry(entry);
   n_events ++;
   if(n_events%100000 == 0)  printf("Processing %2d00.0 k events!\n", (n_events/100000));

   TLorentzVector evts;
   evts.SetPxPyPzE(Px, Py, Pz, Pe);

   if(evts.Pt() > 1000){
     n_cuts++;
     histMass->Fill(Mass);
     histPt->Fill(evts.Pt());
     histRapidity->Fill(evts.Rapidity());
   }

   return kTRUE;
}

void BMass_Ana::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.
   cout<<"Total number of events is "<<n_events<<endl;
   cout<<"After Selection is "<<n_cuts<<endl;
}

void BMass_Ana::Terminate()
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.
  myRootFile->Write();
  myRootFile->Close();
}
