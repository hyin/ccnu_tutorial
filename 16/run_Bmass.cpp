///////////////////////////////
// Main Program
///////////////////////////////
#include <iostream>
#include <fstream>
#include <stdlib.h>  //atoi
#include <unistd.h>  //getopt
#include <cstdlib>

#include <TROOT.h>
#include <TChain.h>
#include <TDirectory.h>
#include <TStyle.h>
#include <TFile.h>
#include <TTree.h>

#include "BMass_Ana.h"

using namespace std;

//
// main function
//
int main(int argc, char** argv) {
  // root style setting
  //gROOT->ProcessLine(".x ../13/lhcbStyle.C");

  //command line parameter defaults
  int nevt = 0;
  string filelist = "file.list";

  //parse command line
  int c;
  while( (c=getopt(argc,argv,"hn:n:f:")) != -1 ){
    switch(c){
    case 'h':
      std::cout <<"Usage: " << argv[0]<<std::endl 
		<<"         [-h] [-n <number_of_events>] " << std::endl
		<<"         [-f filelist] " << std::endl
		<<"Example:"<<std::endl
		<<"   ./run_bmass -f file.list "<<std::endl<<std::endl;
      exit(0);
      break;
    case 'n':
      nevt = atoi(optarg);
      if (nevt < 0) nevt = 0;
      break;
    case 'f':
      filelist = optarg;
      break;
    }
  }
  
  // get input and output files from parameters.rc
  TChain *ch = new TChain("Tuple");

  // input root file
  TString input_roottuple;

  // processor class
  BMass_Ana *bmass_sel = new BMass_Ana;

  std::cout << "===========================================" << "\n"
            << "           Reading Files                   " << "\n"
            << "===========================================" << std::endl;

  // read in the root file(s)
  ifstream fin;
  fin.open(filelist.c_str());
  Int_t sum=0;
  
  while(fin>>input_roottuple){
    
    TFile *newROOTFile= new TFile(input_roottuple);
    TTree *SelEventROOTTree = (TTree *)newROOTFile->Get("Tuple");
    
    if(!SelEventROOTTree){
      std::cout << "Can not open ROOT file: " << input_roottuple << std::endl;
    } else {
      std::cout << "File: " << input_roottuple << ", Entries = " << SelEventROOTTree->GetEntries() << std::endl;
      ch->Add(input_roottuple);
      sum++;
    }
    
    delete SelEventROOTTree;
    delete newROOTFile;
  }
  fin.close();
  
  // print out the number of events 
  std::cout << "===========================================" << "\n"
	    << "       " << sum << " file(s) is(are) merged" << "      " << "\n"
	    << "===========================================" << std::endl;
  
  std::cout << "===========================================" << "\n"
	    << "           Analyzing  Files                   " << "\n"
	    << "===========================================" << std::endl;
  
  int runevt = ch->GetEntries();
  std::cout << "Entries: " << runevt << std::endl;

  // number of events to be skimmed
  if (nevt<runevt&&nevt!=0){
    runevt = nevt;
  }
  
  // study the Jpsi at here
  ch->Process(bmass_sel, "", runevt);
}
