#include "iostream"
using namespace std;

int main(){
  double num_array[5] = {0.001, 100.0, 231.8759, 1.2, 43.6};

  // c++ default print out
  for(int i = 0; i < 5; i++){
    cout<< num_array[i] <<endl;
  }
  cout<<endl;

  // c style print out
  for(int i = 0; i < 5; i++){
    printf("%.2f \n", num_array[i]);
  }
  cout<<endl;
 
  // with fixed width 
  for(int i = 0; i < 5; i++){
    printf("%8.2f \n", num_array[i]);
  }
  cout<<endl;
}
