#include <iostream>
using namespace std;
  
int main(){
  unsigned short int myAge = 5, yourAge = 10;
  unsigned short int *pAge = &myAge;  // a pointer

  cout << "myAge:\t" << myAge;
  cout << "\t\tyourAge:\t" << yourAge << endl;
  cout << "&myAge:\t" << &myAge;
  cout << "\t&yourAge:\t" << &yourAge << endl;

  cout << "pAge:\t" << pAge << endl;
  cout << "*pAge:\t" << *pAge << endl << endl;

  pAge = &yourAge;       // reassign the pointer

  cout << "myAge:\t" << myAge;
  cout << "\t\tyourAge:\t" << yourAge << endl;
  cout << "&myAge:\t" << &myAge;
  cout << "\t&yourAge:\t" << &yourAge << endl;

  cout << "pAge:\t" << pAge << endl;
  cout << "*pAge:\t" << *pAge << endl << endl;

  cout << "&pAge:\t" << &pAge << endl;
  return 0;
}
