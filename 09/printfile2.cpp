#include "iostream"
#include "fstream"
using namespace std;

int main(){
  double num_array[5] = {0.001, 100.0, 231.8759, 1.2, 43.6};

  // output to a file
  ofstream out("out3.txt");
  // c++ default print out
  for(int i = 0; i < 5; i++){
    out<< num_array[i] <<"  ";
  }
  out<<endl;
  out.close();

  // c style print out
  FILE* out2;
  out2 = fopen("out4.txt", "w");

  fprintf(out2, "%.2f %.2f %.2f %.2f %.2f \n", 
          num_array[0], num_array[1], 
          num_array[2], num_array[3], 
          num_array[4]);
 
  fclose(out2);
}
