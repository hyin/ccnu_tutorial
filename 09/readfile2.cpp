#include "iostream"
#include "fstream"
using namespace std;

int main(){
  // input from a file
  ifstream input("../10/data_input.txt");

  // c++ default print out
  while (!input.eof()){
    string name_tmp;
    double px_tmp, py_tmp, pz_tmp, pe_tmp;

    input >> name_tmp >> px_tmp >> py_tmp >> pz_tmp >> pe_tmp;
    cout << px_tmp << endl;
  }
}
