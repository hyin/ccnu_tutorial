#include <iostream>
using namespace std;
  
int main(){
  int myAge;            // a variable
  int *pAge = NULL;     // a pointer

  myAge = 5;
  pAge = &myAge;     // assign address of myAge to pAge
  cout << "myAge: " << myAge << endl;
  cout << "*pAge: " << *pAge << endl << endl;

  cout << "*pAge = 7" << endl;
  *pAge = 7;         // sets myAge to 7
  cout << "*pAge: " << *pAge << endl;
  cout << "myAge: " << myAge << endl << endl;

  cout << "myAge = 9" << endl;
  myAge = 9;
  cout << "myAge: " << myAge << endl;
  cout << "*pAge: " << *pAge << endl;

  return 0;
}
