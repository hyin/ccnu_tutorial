#include "iostream"
#include "fstream"
using namespace std;

int main(){
  double num_array[5] = {0.001, 100.0, 231.8759, 1.2, 43.6};

  // output to a file
  ofstream out("out.txt");
  // c++ default print out
  for(int i = 0; i < 5; i++){
    out<< num_array[i] <<endl;
  }
  out<<endl;
  out.close();

  // c style print out
  FILE* out2;
  out2 = fopen("out2.txt", "w");

  for(int i = 0; i < 5; i++){
    fprintf(out2, "%8.2f \n", num_array[i]);
  }
  fclose(out2);
}
