#include "iostream"
#include "fstream"
using namespace std;

int main(){
  // input from a file
  ifstream input("../10/data_input.txt");

  // c++ default print out
  string buffer;
  while (getline(input, buffer)){
    double tmp_px, tmp_py, tmp_pz, tmp_pe;
    sscanf(buffer.c_str(), "%*s %lf %lf %lf %lf",
           &tmp_px, &tmp_py, &tmp_pz, &tmp_pe);
    cout << tmp_px << endl;
  }
}
