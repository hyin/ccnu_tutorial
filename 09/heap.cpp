#include <iostream>
using namespace std;
  
int main(){
  int localVariable = 5;
  int *pLocal= &localVariable;
  int *pHeap = new int;
  if (pHeap == NULL){
    cout << "Error! No memory for pHeap!!" << endl;
    return 1;
  }
  *pHeap = 7;
  std::cout << "localVariable: " << localVariable << endl;
  std::cout << "*pLocal: " << *pLocal << endl;
  std::cout << "*pHeap: " << *pHeap << endl;
  delete pHeap;
  pHeap = new int;
  if (pHeap == NULL){
    cout << "Error! No memory for pHeap!!" << endl;
    return 1;
  }
  *pHeap = 9;
  cout << "*pHeap: " << *pHeap << endl;
  delete pHeap;
  return 0;
}
